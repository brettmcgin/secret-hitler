package com.brettmcgin.secrethitler.monolog

import com.brettmcgin.secrethitler.GameSize
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test


class MonologTest {
    private val nonSmallFirstSegments = listOf(first7100, first7101, first7102)

    @Test
    fun `Small Game has valid monolog`() {
        with(GameSize.Small.toMonolog()) {
            assertEquals(6, segments.size)

            assertEquals(first560, segments[0])
            assertEquals(second560, segments[1])
            assertEquals(silence, segments[2])
            assertTrue(segments[3] in fourthSegments)
            assertEquals(silence, segments[4])
            assertTrue(segments[5] in sixthSegments)
        }
    }

    @Test
    fun `Medium Game has valid monolog`() {
        with(GameSize.Medium.toMonolog()) {
            assertEquals(6, segments.size)

            assertTrue(segments[0] in nonSmallFirstSegments)
            assertEquals(second780, segments[1])
            assertTrue(segments[2] in thirdSegments)
            assertTrue(segments[3] in fourthSegments)
            assertTrue(segments[4] in fifthSegments)
            assertTrue(segments[5] in sixthSegments)
        }
    }

    @Test
    fun `Large Game has valid monolog`() {
        with(GameSize.Large.toMonolog()) {
            assertEquals(6, segments.size)

            assertTrue(segments[0] in nonSmallFirstSegments)
            assertEquals(second9100, segments[1])
            assertTrue(segments[2] in thirdSegments)
            assertTrue(segments[3] in fourthSegments)
            assertTrue(segments[4] in fifthSegments)
            assertTrue(segments[5] in sixthSegments)
        }
    }
}
