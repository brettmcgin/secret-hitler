package com.brettmcgin.secrethitler.ui.theme

import androidx.compose.material.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import com.brettmcgin.secrethitler.R

private val fontFamily = FontFamily(listOf(Font(R.font.ahamono_monospaced)))

val Typography = Typography(
        body1 = TextStyle(
                fontFamily = fontFamily,
                fontWeight = FontWeight.Bold,
                fontSize = 24.sp,
                color = SecretHitlerText,
        ),
        body2 = TextStyle(
                fontFamily = fontFamily,
                fontWeight = FontWeight.Bold,
                fontSize = 16.sp,
                color = SecretHitlerText,
        )
)
