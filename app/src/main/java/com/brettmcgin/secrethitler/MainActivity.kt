package com.brettmcgin.secrethitler

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.IconButton
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.brettmcgin.secrethitler.monolog.toMonolog
import com.brettmcgin.secrethitler.monologplayer.toPlayer
import com.brettmcgin.secrethitler.ui.theme.SecretHitlerSurface
import com.brettmcgin.secrethitler.ui.theme.SecretHitlerText
import com.brettmcgin.secrethitler.ui.theme.SecretHitlerTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            SecretHitlerTheme {
                window.statusBarColor = SecretHitlerSurface.toArgb()

                Surface(Modifier.fillMaxSize(), color = SecretHitlerText) {
                    var player by remember {
                        mutableStateOf(GameSize.Small.toMonolog().toPlayer(applicationContext))
                    }

                    fun GameSize.startNewMonolog() {
                        player.stop()
                        player = toMonolog().toPlayer(applicationContext)
                        player.start()
                    }

                    var currentRecord by rememberSaveable { mutableStateOf(GameSize.Small) }
                    val isPlaying by player.isPlaying.collectAsState(false)

                    fun pauseOrNew(gameSize: GameSize) {
                        if (currentRecord == gameSize) {
                            player.pause()
                        } else currentRecord = gameSize.also { it.startNewMonolog() }
                    }

                    Content(
                        gameSize = currentRecord,
                        isPlaying = isPlaying,
                        onSmallGameClicked = { pauseOrNew(GameSize.Small) },
                        onMediumGameClicked = { pauseOrNew(GameSize.Medium) },
                        onLargeGameClicked = { pauseOrNew(GameSize.Large) },
                    )
                }
            }
        }
    }
}

@Composable
fun Content(
    gameSize: GameSize,
    isPlaying: Boolean = false,
    onSmallGameClicked: () -> Unit,
    onMediumGameClicked: () -> Unit,
    onLargeGameClicked: () -> Unit,
) = Box(
    Modifier
        .fillMaxSize()
        .padding(16.dp)
        .background(SecretHitlerSurface),
    Alignment.Center
) {
    Column {
        Title()
        SubTitle()

        VinylRecord(gameSize == GameSize.Small && isPlaying) { onSmallGameClicked() }
        VinylRecord(gameSize == GameSize.Medium && isPlaying) { onMediumGameClicked() }
        VinylRecord(gameSize == GameSize.Large && isPlaying) { onLargeGameClicked() }
    }
}

@Composable
fun Title() = Image(
    painterResource(R.drawable.secrethitler),
    stringResource(R.string.app_name),
)

@Composable
fun SubTitle() = Column {
    Text(stringResource(R.string.narration).uppercase())
    Text(stringResource(R.string.by_wil_wheaton))
}

@Composable
fun VinylRecord(
    isPlaying: Boolean = false,
    onCLick: () -> Unit,
) {
    IconButton(onClick = { onCLick() }) {
        val infiniteTransition = rememberInfiniteTransition()

        val infiniteAngle by infiniteTransition.animateFloat(
            initialValue = 0F,
            targetValue = 360F,
            animationSpec = infiniteRepeatable(
                animation = tween(2000, easing = LinearEasing),
                repeatMode = RepeatMode.Restart,
            )
        )

        val modifier = Modifier.graphicsLayer { rotationZ = infiniteAngle }.takeIf { isPlaying }

        Image(
            painter = painterResource(id = R.drawable.record),
            contentDescription = null,
            modifier = modifier ?: Modifier
        )
    }
}
