package com.brettmcgin.secrethitler.monologplayer

import android.content.Context
import android.media.MediaPlayer
import com.brettmcgin.secrethitler.monolog.Monolog
import com.brettmcgin.secrethitler.monolog.Segment
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow

interface Player {
    val isPlaying: Flow<Boolean>
    fun start()
    fun stop()
    fun pause()
}

fun Monolog.toPlayer(context: Context): Player = object : Player {
    private var position = 0
    private var mediaPlayer: MediaPlayer? = null
    private var _isPlaying = MutableStateFlow(mediaPlayer?.isPlaying ?: false)
    override val isPlaying: Flow<Boolean> get() = _isPlaying

    init {
        0.setMediaPlayer()
    }

    override fun start() {
        mediaPlayer?.start()
        _isPlaying.tryEmit(true)
    }

    override fun stop() {
        position = 0
        mediaPlayer?.stop()
        _isPlaying.tryEmit(false)
    }

    override fun pause() {
        if (_isPlaying.value) {
            mediaPlayer?.pause()
            _isPlaying.tryEmit(false)
        } else {
            mediaPlayer?.start()
            _isPlaying.tryEmit(true)
        }
    }

    private fun Int.setMediaPlayer() {
        segments.getOrNull(coerceAtMost(segments.size).coerceAtLeast(0))
            ?.let { mediaPlayer = it.newPlayer() }
    }

    private fun Segment.newPlayer() =
        MediaPlayer.create(context, id).apply { setOnCompletionListener { next() } }

    private fun next() {
        position += 1
        if (position > segments.size) {
            position = 0
            mediaPlayer?.stop()
            _isPlaying.tryEmit(false)
            return
        }

        position.setMediaPlayer()
        mediaPlayer?.start()
        _isPlaying.tryEmit(true)
    }
}
