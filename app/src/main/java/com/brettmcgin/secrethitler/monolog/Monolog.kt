package com.brettmcgin.secrethitler.monolog

import com.brettmcgin.secrethitler.GameSize
import com.brettmcgin.secrethitler.GameSize.Large
import com.brettmcgin.secrethitler.GameSize.Medium
import com.brettmcgin.secrethitler.GameSize.Small

data class Monolog(val segments: List<Segment>)

fun GameSize.toMonolog() = when (this) {
    Small -> gameDialog(5, 6)
    Medium -> gameDialog(7, 8)
    Large -> gameDialog(9, 10)
}.let(::Monolog)

private fun gameDialog(minPlayers: Int, maxPlayers: Int) = listOf(
    firstSegments,
    secondSegments,
    thirdSegments,
    fourthSegments,
    fifthSegments,
    sixthSegments,
).map { segments ->
    val choices = segments.filter(minPlayers, maxPlayers)

    if (choices.isEmpty()) silence else choices.random()
}

private fun List<Segment>.filter(minPlayers: Int, maxPlayers: Int) =
    filter { minPlayers >= it.minPlayers && maxPlayers <= it.maxPlayers }
