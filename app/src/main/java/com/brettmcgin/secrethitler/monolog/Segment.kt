package com.brettmcgin.secrethitler.monolog

import com.brettmcgin.secrethitler.R

data class Segment(
    val minPlayers: Int = 5,
    val maxPlayers: Int = 6,
    val variant: Int = 0,
    val id: Int = R.raw.segment_0_5_6_0,
)

internal val first560 = Segment(5, 6, 0, R.raw.segment_0_5_6_0)
internal val first7100 = Segment(7, 10, 0, R.raw.segment_0_7_10_0)
internal val first7101 = Segment(7, 10, 1, R.raw.segment_0_7_10_1)
internal val first7102 = Segment(7, 10, 2, R.raw.segment_0_7_10_2)

internal val second560 = Segment(5, 6, 0, R.raw.segment_1_5_6_0)
internal val second780 = Segment(7, 8, 0, R.raw.segment_1_7_8_0)
internal val second9100 = Segment(9, 10, 0, R.raw.segment_1_9_10_0)

private val third7100 = Segment(7, 10, 0, R.raw.segment_2_7_10_0)
private val third7101 = Segment(7, 10, 1, R.raw.segment_2_7_10_1)
private val third7102 = Segment(7, 10, 2, R.raw.segment_2_7_10_2)
private val third7103 = Segment(7, 10, 3, R.raw.segment_2_7_10_3)
private val third7104 = Segment(7, 10, 4, R.raw.segment_2_7_10_4)

private val fourth5100 = Segment(5, 10, 0, R.raw.segment_3_5_10_0)
private val fourth5101 = Segment(5, 10, 1, R.raw.segment_3_5_10_1)

private val fifth7100 = Segment(7, 10, 0, R.raw.segment_4_7_10_0)
private val fifth7101 = Segment(7, 10, 1, R.raw.segment_4_7_10_1)

private val sixth5100 = Segment(5, 10, 0, R.raw.segment_5_5_10_0)
private val sixth5101 = Segment(5, 10, 1, R.raw.segment_5_5_10_1)
private val sixth5102 = Segment(5, 10, 2, R.raw.segment_5_5_10_2)
private val sixth5103 = Segment(5, 10, 3, R.raw.segment_5_5_10_3)
private val sixth5104 = Segment(5, 10, 4, R.raw.segment_5_5_10_4)

internal val silence = Segment(5, 10, 0, R.raw.silence)

internal val firstSegments = listOf(first560, first7100, first7101, first7102)
internal val secondSegments = listOf(second560, second780, second9100)
internal val thirdSegments = listOf(third7100, third7101, third7102, third7103, third7104)
internal val fourthSegments = listOf(fourth5100, fourth5101)
internal val fifthSegments = listOf(fifth7100, fifth7101)
internal val sixthSegments = listOf(sixth5100, sixth5101, sixth5102, sixth5103, sixth5104)
