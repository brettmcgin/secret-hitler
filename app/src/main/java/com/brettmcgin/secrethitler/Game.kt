package com.brettmcgin.secrethitler

enum class GameSize {
    Small,
    Medium,
    Large
}
